import { Given, When, Then, then } from "cypress-cucumber-preprocessor/steps"
import Home from '../pageobjects/home'
const home = new Home();

Given('acesso o site google', ()=>{
    cy.visit('https://www.google.com')
}); 

And('teste èéã', ()=>{
    return true
})

When('pesquiso por {string}', (word)=>{
   home.verifyByElementGogle()
   home.searchWordKeyGoogle(word)
})

Then('acesso o primeiro resultado', ()=>{
    home.clickFirst()
})
