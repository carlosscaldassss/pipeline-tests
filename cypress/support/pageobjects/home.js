import Base from '../../../_base'
const base = new Base()
const inputSearch = `//input[@name="q"]`
const firstResult = `//div[@class="yuRUbf"]`

export default class Home {
    verifyByElementGogle(){
        base.verifyElementExist(inputSearch)
    }

    searchWordKeyGoogle(word){
        base.inputValue(inputSearch ,word)
    }

    clickFirst(){
        base.clickFirstElementList(firstResult)
    }
}