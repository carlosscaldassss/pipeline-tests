/// <reference types="Cypress" />

export default class Base {
    verifyElementExist(element) {
        cy.xpath(element).should('be.visible')
    }
    
    inputValue(element, value){
        cy.xpath(element).type(`${value}{enter}`)
    }

    clickFirstElementList(element){
        cy.xpath(element).first().click({force:true})
    }
}